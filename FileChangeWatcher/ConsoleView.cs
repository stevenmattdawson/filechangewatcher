﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.Abstractions;
using System.Linq;

namespace FileChangeWatcher {
	public class Consoleview {
		[STAThread]
		static void Main(string[] args) {
			//Show Help
			if (args.Length == 1 && (args[0] == "?" || args[0].ToLower() == "help")) {
				ShowHelp();
				return;
			}

			(string path, string extension) arguments = ("", "");

			//Catch lazy user and "Clicked from File Explorer" Case
			if (args.Length < 2) {
				arguments = GetArguments(arguments);
			}
			else {
				arguments = (args[0], args[1]);
			}

			(string path, string extension) validArgs;

			//Loop until we have a valid file and a valid file extension
			while (!ValidateArguments(arguments, new FileSystem(), out validArgs)) {
				arguments = validArgs;
				arguments = GetArguments(arguments);
			}

			Console.WriteLine(Strings.ParametersValidated);
			Trace.Listeners.Add(new ConsoleTraceListener());

			Console.Write(Strings.InitializingFileWatcher);

			var watcher = new LineChangeWatcher(validArgs.path, validArgs.extension, 10000);
			Console.WriteLine(string.Format(Strings.ReportWatchingMessage, validArgs.path, validArgs.extension));
			
			watcher.StartWatching();

			Console.ReadLine();
		}

		private static (string path, string extension) GetArguments((string path, string extension) arguments) {
			if (string.IsNullOrEmpty(arguments.path)) {
				Console.Write(Strings.AskForFilePath);
				arguments.path = Console.ReadLine();
			}
			if (string.IsNullOrEmpty(arguments.extension)) {
				Console.Write(Strings.AskForException);
				arguments.extension = Console.ReadLine();
			}

			return arguments;
		}


		private static void ShowHelp()
		{
			Console.WriteLine(Strings.HelpMessage);
		}
			
		/// <summary>
		/// Validates the arguments.
		/// </summary>
		/// <param name="args">current attempt at valid arguements</param>
		/// <param name="io">injectable filesystem for unit testing.</param>
		/// <param name="validArgs">validated arguements</param>
		/// <returns>if true, validArgs will contain data usable by the LineChangeWatcher, if false, validArgs will be the emptystring for each arguement that was not valid</returns>
		public static bool ValidateArguments((string path, string extension) args, IFileSystem io, out (string path, string extension) validArgs) {
			validArgs = ("", "");			

			if (io.Directory.Exists(args.path)) {
				validArgs.path = args.path;
			}
			else {
				Console.WriteLine(string.Format(Strings.NotAValidPathMessage, args.path));
			}

			if (string.IsNullOrEmpty(args.extension)) {
				Console.WriteLine(string.Format(Strings.NotAValidExtensionMessage, args.extension));
				return false;
			}

			List<char> invalid = io.Path.GetInvalidPathChars().ToList();

			invalid.AddRange("/\\:*?\"<>|".ToCharArray());
			if (args.extension[0] == '*') {
				args.extension = args.extension.Substring(1);
			}

			if (!args.extension.Any(s => invalid.Contains(s))) {
				validArgs.extension = args.extension;
				if (validArgs.extension[0] != '.') {
					validArgs.extension = $".{validArgs.extension}";
				}
			}

			if (string.IsNullOrEmpty(validArgs.extension)) {
				Console.WriteLine(string.Format(Strings.NotAValidExtensionMessage, args.extension));
			}

			return validArgs.path != "" && validArgs.extension != "";
		}
	}
}

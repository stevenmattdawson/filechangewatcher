﻿using System;
using System.Windows.Threading;

namespace FileChangeWatcher {
	public class UIDispatcher : IDispatcher {

		Dispatcher _threadSafeDispatcher;

		public UIDispatcher(Dispatcher threadSafeDispatcher) {
			_threadSafeDispatcher = threadSafeDispatcher;
		}

		public void Dispatch(Action action) {
			_threadSafeDispatcher.BeginInvoke(DispatcherPriority.Background, action);
		}
	}
}

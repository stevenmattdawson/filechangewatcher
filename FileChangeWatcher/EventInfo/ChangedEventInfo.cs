﻿namespace FileChangeWatcher.EventInfo {
	public class ChangedEventInfo : EventInfoBase {

		public ChangedEventInfo(string filename, int currentLineCount, int previousLineCount) {
			FileName = filename;
			CurrentLineCount = currentLineCount;
			PreviousLineCount = previousLineCount;
		}
		public override string ToString() {
			return string.Format(Strings.FileChangedResult, FileName, LineChangeWatcher.FormatDifference(CurrentLineCount, PreviousLineCount));
		}
	}
}

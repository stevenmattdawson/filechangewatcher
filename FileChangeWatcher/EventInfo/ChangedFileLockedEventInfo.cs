﻿namespace FileChangeWatcher.EventInfo {
	public class ChangedFileLockedEventInfo : FileLockedEventInfo {
		public ChangedFileLockedEventInfo(string filename) : base(filename) { }

		public override string ToString() {
			return string.Format(Strings.ChangedFileLockedMessage, FileName);
		}
	}
}

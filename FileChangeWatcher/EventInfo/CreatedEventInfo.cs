﻿namespace FileChangeWatcher.EventInfo {
	public class CreatedEventInfo : EventInfoBase {


		public CreatedEventInfo(string filename, int currentLineCount) {
			FileName = filename;
			CurrentLineCount = currentLineCount;
		}

		public override string ToString() {
			return string.Format(Strings.FileCreatedResult, FileName, CurrentLineCount);
		}
	}
}

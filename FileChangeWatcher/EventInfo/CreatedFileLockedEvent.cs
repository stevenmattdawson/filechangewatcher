﻿namespace FileChangeWatcher.EventInfo {
	public class CreatedFileLockedEvent : FileLockedEventInfo {
		public CreatedFileLockedEvent(string filename) : base(filename) { }

		public override string ToString() {
			return string.Format(Strings.CreatedFileLockedMessage, FileName);
		}
	}
}

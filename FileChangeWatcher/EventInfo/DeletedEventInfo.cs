﻿namespace FileChangeWatcher.EventInfo {
	public class DeletedEventInfo : EventInfoBase {
		public DeletedEventInfo(string filename) {
			FileName = filename;
		}
		public override string ToString() {
			return string.Format(Strings.FileDeletedResult, FileName);
		}
	}
}

﻿using System;

namespace FileChangeWatcher.EventInfo {

	public abstract class EventInfoBase : EventArgs {
		public string FileName = "";
		public int CurrentLineCount = -1;
		public int PreviousLineCount = -1;
		public bool FileLocked = false;

		public override bool Equals(object obj) {
			return this.ToString().Equals(obj.ToString());
		}

		public override int GetHashCode() {
			return base.GetHashCode();
		}
	}
}

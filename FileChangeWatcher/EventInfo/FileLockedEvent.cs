﻿namespace FileChangeWatcher.EventInfo {
	public class FileLockedEventInfo : EventInfoBase {
		public FileLockedEventInfo(string filename) {
			FileName = filename;
		}

		public override string ToString() {
			return string.Format(Strings.FileLockedMessage, FileName);
		}
	}
}

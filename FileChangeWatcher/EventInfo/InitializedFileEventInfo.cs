﻿using System;
using System.IO;

namespace FileChangeWatcher.EventInfo {
	/// <summary>
	/// Container class for tracking file data.
	/// </summary>
	public class InitializedFileEventInfo {
		public int LineCount;
		public DateTime LastWriteTime;

		private string _filename;
		public string FileName {
			get => _filename;
		}

		private string _filepath;
		public string FilePath {
			get => _filepath;
			set {
				if (File.Exists(value)) {
					_filename = Path.GetFileName(value);
					_filepath = value;
				}
				else {
					_filename = "";
					_filepath = "";
				}
			}
		}

		public InitializedFileEventInfo(int lineCount, DateTime lastWriteTime, string filePath) {
			LineCount = lineCount;
			LastWriteTime = lastWriteTime;
			FilePath = filePath;
		}
	}
}

﻿namespace FileChangeWatcher.EventInfo {
	class InvalidFileEventInfo : EventInfoBase{

		public InvalidFileEventInfo(string filename) {
			FileName = filename;
		}
		public override string ToString() {
			return string.Format(Strings.InvalidFileType, FileName);
		}
	}
}

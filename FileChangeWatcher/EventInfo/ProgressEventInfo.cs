﻿namespace FileChangeWatcher.EventInfo {
	public class ProgressEventInfo {
		public string Message;

		public ProgressEventInfo(int progress, string message) {
			ProgressPercentage = progress;
			Message = message;
		}

		//from 0 to 100
		private int _progressPercentage;
		public int ProgressPercentage {
			get => _progressPercentage;
			set {
				if (value < 0) {
					_progressPercentage = 0;
				}
				else if (value >= 100) {
					_progressPercentage = 100;
				}
				else {
					_progressPercentage = value;
				}
			}
		}
	}
}

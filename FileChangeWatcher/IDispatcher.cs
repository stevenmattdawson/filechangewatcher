﻿using System;

namespace FileChangeWatcher {
	public interface IDispatcher {
		void Dispatch(Action action);
	}
}

﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Abstractions;
using System.Linq;
using System.Security;
using System.Threading.Tasks;
using System.Timers;
using FileChangeWatcher.EventInfo;
using Timer = System.Timers.Timer;

namespace FileChangeWatcher {
	/// <summary>
	/// Watches files for a change in thier last write time, then outputs a trace message with a Created, Changed, or Deleted message and (CR, LF) based linecount information.
	/// </summary>
	public class LineChangeWatcher
	{
		private readonly IFileSystem _system;
		private ConcurrentDictionary<string, InitializedFileEventInfo> _fileInfo = new ConcurrentDictionary<string, InitializedFileEventInfo>();
		private Timer timer;
		private bool reportTimer = false;
		private string _path;

		public event EventHandler<EventInfoBase> FileChangedEvent;
		public event EventHandler<InitializedFileEventInfo> FileInitializedEvent;
		public event EventHandler<ProgressEventInfo> UpdateProgressEvent;

		/// <summary>
		/// Path to watch for file changes.
		/// </summary>
		public string Path {
			get {
				return _path;
			}
			set {
				_path = _system.Path.GetFullPath(value);
			}
		}
		string _extension;

		/// <summary>
		/// Used in production to construct the LineChangeWatcher
		/// </summary>
		/// <param name="path">The path to the directory that should be watched. Can be relative, absolute, or UNC</param>
		/// <param name="extension">file extension for an ASCII or UTF-8 files that should be watched.</param>
		/// <param name="interval">interval to trigger each file system check</param>
		public LineChangeWatcher(string path, string extension, int interval = 10000) : this(path, extension, new FileSystem(), interval) { }

		/// <summary>
		/// Used specifically for Unit Testing as it allows for the injection of a Mock FileSystem.
		/// </summary>
		/// <param name="path">The path to the directory that should be watched. Can be relative, absolute, or UNC</param>
		/// <param name="extension">file extension for an ASCII or UTF-8 files that should be watched.</param>
		/// <param name="fileSystem">File System to be mocked. if left out, will use the actual file system.</param>
		/// <param name="interval">interval to trigger each file system check</param>
		public LineChangeWatcher(string path, string extension, IFileSystem fileSystem, int interval = 10000) {
			_system = fileSystem;
			Path = path;
			_extension = extension;
			timer = new Timer(interval);
			timer.Elapsed += CheckFilesAync;
			timer.AutoReset = true;
			timer.Enabled = false;
		}


		/// <summary>
		/// Tells the watcher to begin watching, starts a timer which will fire at the set interval.
		/// </summary>
		/// <param name="report"></param>
		public void StartWatching(bool report = false) {
			reportTimer = report;
			timer.Enabled = true;
			timer.Start();
			CheckFilesAync(null, null);
		}


		/// <summary>
		/// Timer.Elapsed Listener than will be fired at consistant intervals
		/// </summary>
		/// <param name="source">Ignored</param>
		/// <param name="e">Ignored</param>
		public async void CheckFilesAync(Object source, ElapsedEventArgs e) {
			if (reportTimer) {
				Trace.WriteLine(string.Format(Strings.CheckingFileSystemMessage, DateTime.Now.ToShortTimeString()));
			}

			await PerformCheckAsync();

			OnRaiseEvent(new ProgressEventInfo(100, string.Format(Strings.LastScan, DateTime.Now)));
		}

		public void StopWatching() {
			timer.Stop();
			OnRaiseEvent(new ProgressEventInfo(0, Strings.StoppedScanning));
			return;
		}

		public bool IsWatching {
			get => timer.Enabled;
		}
		/// <summary>
		/// Queries all the files in the watched directory for created, changed, and deleted files.
		/// </summary>
		/// <returns>A boolion value signifying success or failure</returns>
		public async Task<bool> PerformCheckAsync() {
			try {
				List<Task<EventInfoBase>> taskList = new List<Task<EventInfoBase>>();
				List<string> potentiallyDeletedFiles = _fileInfo.Keys.ToList();
				List<string> files = GetFilesList().ToList(); //makes debugging easier
				foreach (var file in files) {
					if (!Watching(file)) {	//File was created
						taskList.Add(ProcessFileCreated(file));
					}
					else if (_fileInfo[file].LineCount == -1) {	//file was locked on initialization
						TryAddFilePath(file);
					}
					else if (_system.File.GetLastWriteTime(file) > _fileInfo[file].LastWriteTime) { //File was changed
							taskList.Add(ProcessFileChanged(file));
					}
					potentiallyDeletedFiles.Remove(file);
				}

				foreach (var file in potentiallyDeletedFiles) {
					if (!_system.File.Exists(file)) { //File was deleted
						taskList.Add(ProcessFileDeleted(file));
					}
				}
				int taskCount = taskList.Count();
				int finishedTasks = 0;
				if (taskList.Any()) {
					EventInfoBase noChanges = null;
					OnRaiseEvent(noChanges);//send null to trigger the refresh.
				}
				while (taskList.Any()) {
					Task<EventInfoBase> finished = await Task.WhenAny(taskList);
					taskList.Remove(finished);
					EventInfoBase result = await finished;
					double progress = ((double)finishedTasks++ / taskCount);
					OnRaiseEvent(new ProgressEventInfo((int)(progress * 100), string.Format(Strings.FinishedProcessing_Of_Files, finishedTasks, taskCount)));
					Trace.WriteLine(result);
					OnRaiseEvent(result);
				}
				return true;
			}
			catch(SecurityException) {
				Trace.WriteLine(string.Format(Strings.InsufficientAccessPermission, Path));
			}
			return false;
		}

		/// <summary>
		/// Outputs a message stating that the given filepath has been created with the number of lines in the file.
		/// If the file is locked, will instead report that the file is inaccessable.
		/// </summary>
		/// <param name="filePath">Path to report as created.</param>
		/// <returns>A task that upon completion will have the result containing the file created message or a message stating the file is inaccessable.</returns>
		public Task<EventInfoBase> ProcessFileCreated(string filePath) {
			return Task.Run(() => {
				string name = _system.Path.GetFileName(filePath);
				
				try {
					int lineCount = GetLineCount(filePath);

					_fileInfo[filePath] = new InitializedFileEventInfo(lineCount, _system.File.GetLastWriteTime(filePath), filePath);
					_fileInfo[filePath].LineCount = lineCount;

					return (EventInfoBase)new CreatedEventInfo(name, lineCount);
				}
				catch (IOException) {
					return new CreatedFileLockedEvent(name);
				}
				catch (ArgumentException) {
					_fileInfo.TryRemove(filePath, out _);
					return new InvalidFileEventInfo(_system.Path.GetFileName(filePath));
				}
			});
		}

		/// <summary>
		/// Outputs a message stating that the given filepath has been changed with the number of lines added or removed from the file.
		/// If the file is locked, will instead report that the file is inaccessable.
		/// </summary>
		/// <param name="filePath">Path to report as created.</param>
		/// <returns>A task that upon completion will have the result containing the file changed message or a message stating the file is inaccessable.</returns>
		public Task<EventInfoBase> ProcessFileChanged(string filePath) {
			return Task.Run(() => {
				string name = _system.Path.GetFileName(filePath);
				try {
					int oldCount = _fileInfo[filePath].LineCount;
					int newCount = GetLineCount(filePath);

					_fileInfo[filePath].LineCount = newCount;
					_fileInfo[filePath].LastWriteTime = _system.File.GetLastWriteTime(filePath);

					string difference = FormatDifference(newCount, oldCount);

					return (EventInfoBase)new ChangedEventInfo(name, newCount, oldCount);
				}
				catch (IOException) {
					return new ChangedFileLockedEventInfo(name);
				}
			});
		}

		public static string FormatDifference(int newCount, int oldCount) {
			if (newCount == oldCount) {
				return "0";
			}
			return (oldCount > newCount) ? $"-{oldCount - newCount}" : $"+{newCount - oldCount}";
		}


		/// <summary>
		/// Outputs a message stating that the given filepath has been deleted.
		/// </summary>
		/// <param name="filePath">Path to report as deleted.</param>
		/// <returns>A task that upon completion will have the result containing the file deleted message</returns>
		public Task<EventInfoBase> ProcessFileDeleted(string filePath) {
			return Task.Run(() => {
				string name = _system.Path.GetFileName(filePath);
				_fileInfo.TryRemove(filePath, out _);

				return (EventInfoBase)new DeletedEventInfo(name);
			});
		}

		public void Initialize() {
			InitializeAsync().Wait();
		}

		private async Task InitializeAsync() {
			List<Task> taskList = new List<Task>();
		
			foreach (var filePath in GetFilesList()) { 
					taskList.Add(Task.Run(() => {
						TryAddFilePath(filePath);
					}));
			}

			int taskCount = taskList.Count();
			int finishedTasks = 0;
			using (var progressbar = new ProgressBar()) {
				while (taskList.Any()) {
					Task finished = await Task.WhenAny(taskList);
					taskList.Remove(finished);
					double progress = ((double)finishedTasks++ / taskCount);
					progressbar.Report(progress);
					OnRaiseEvent(new ProgressEventInfo((int)(progress * 100), string.Format(Strings.Initialized_Of_Files, finishedTasks, taskCount)));
					if (finished.IsFaulted) {
						Console.WriteLine(finished.Exception.Message);
					}
				}
			}
			OnRaiseEvent(new ProgressEventInfo(100, "Finished"));
			Console.WriteLine(Strings.FinishedInitialization);
		}

		private void TryAddFilePath(string filePath) {
			try {
				if (_system.File.Exists(filePath)) {
					_fileInfo[filePath] = new InitializedFileEventInfo(GetLineCount(filePath), _fileInfo.ContainsKey(filePath) ? _fileInfo[filePath].LastWriteTime : _system.File.GetLastWriteTime(filePath), filePath);
				}
				else {
					_fileInfo.TryRemove(filePath, out _);
				}
			}
			catch (IOException) {
				_fileInfo[filePath] = new InitializedFileEventInfo(-1, _system.File.GetLastWriteTime(filePath), filePath);
			}
			catch (ArgumentException) {
				_fileInfo.TryRemove(filePath, out _);
			}
			OnRaiseEvent(_fileInfo[filePath]);
		}

		private int GetLineCount(string filePath) {
			int count = 0;
			const int charBufferSize = 4096;
			try {
				using (Stream stream = _system.FileStream.Create(filePath, FileMode.Open, FileAccess.Read, FileShare.None)) {
					using (BinaryReader br = new BinaryReader(stream)) {
						bool foundCR = false;
						while (true) {
							char[] chunk = br.ReadChars(charBufferSize);
							if (chunk.Length == 0) {
								break;
							}
							else if (count == 0) {
								count = 1; //we've proven there is at least one line in the file, wether we find (CR LF) or not. All future (CR LF) are counted as extra lines.
							}
							foreach (var bite in chunk) {
								if (bite == '\r') {
									foundCR = true;
								}
								else if (foundCR && bite == '\n') {
									count++;
									foundCR = false;
								}
								else {
									foundCR = false;
								}
							}
						}
					}
				}
			}
			catch (ArgumentException) {
				//handles unsupported file types gracefully (like .png, .exe, etc)
				_fileInfo.TryRemove(filePath, out _);
			}

			return count;
		}

		
		protected virtual void OnRaiseEvent(EventInfoBase info) {
			FileChangedEvent?.Invoke(this, info);
		}

		protected virtual void OnRaiseEvent(InitializedFileEventInfo info) {
			FileInitializedEvent?.Invoke(this, info);
		}

		protected virtual void OnRaiseEvent(ProgressEventInfo info) {
			UpdateProgressEvent?.Invoke(this, info);
		}

		private IEnumerable<string> GetFilesList() {
			return _system.Directory.GetFiles(Path).Where(file => Similiar(_system.Path.GetExtension(file), _extension));
		}

		private bool Similiar(string one, string two) {
			return one.ToLower().Equals(two.ToLower());
		}

		private bool Watching(string filepath) {
			foreach (string key in _fileInfo.Keys) {
				if (Similiar(key, filepath)) {
					return true;
				}
			}
			return false;
		}
	}
}

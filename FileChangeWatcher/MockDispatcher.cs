﻿using System;

namespace FileChangeWatcher {
	public class MockDispatcher : IDispatcher {
		public void Dispatch(Action action) {
			action.Invoke();
		}
	}
}

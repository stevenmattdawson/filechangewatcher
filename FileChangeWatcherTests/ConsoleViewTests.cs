﻿using FileChangeWatcher;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO.Abstractions;
using Tests;


namespace FileChangeWatcher_UnitTests {

	[TestClass]
	public class ConsoleViewTests {
		IFileSystem mockRelativeFileSystem;
		IFileSystem mockAbsoluteFileSystem;
		IFileSystem mockUNCFileSystem;

		[TestInitialize]
		public void Setup() {
			mockRelativeFileSystem = TestData.GetMockFileSystem(TestData.PathType.Relative);
			mockAbsoluteFileSystem = TestData.GetMockFileSystem(TestData.PathType.Absolute);
			mockUNCFileSystem = TestData.GetMockFileSystem(TestData.PathType.UNC);
		}

		[TestMethod]
		public void Test_Invalid_ValidateArgs_Relative() {
			//Test boolean return value
			Assert.IsFalse(Consoleview.ValidateArguments((null, null), mockRelativeFileSystem, out _));
			Assert.IsFalse(Consoleview.ValidateArguments((null, "not an \\extension"), mockRelativeFileSystem, out _));
			Assert.IsFalse(Consoleview.ValidateArguments(("not a directory path", null), mockRelativeFileSystem, out _));
			Assert.IsFalse(Consoleview.ValidateArguments(("not a directory path", "not an ?ension"), mockRelativeFileSystem, out _));
			Assert.IsFalse(Consoleview.ValidateArguments(("not a directory path", TestData.txtExt), mockRelativeFileSystem, out _));
			Assert.IsFalse(Consoleview.ValidateArguments(($"{TestData.relativePath}{"\\Fail.txt"}", "not an \\extension"), mockRelativeFileSystem, out _));
			Assert.IsFalse(Consoleview.ValidateArguments(($"{TestData.relativePath}{"\\Fail.txt"}", TestData.txtExt), mockRelativeFileSystem, out _));
			Assert.IsFalse(Consoleview.ValidateArguments(($"{TestData.absolutePath}{"\\Fail.txt"}", TestData.txtExt), mockRelativeFileSystem, out _));
			Assert.IsFalse(Consoleview.ValidateArguments(($"{TestData.uncPath}{"\\Fail.txt"}", TestData.txtExt), mockRelativeFileSystem, out _));

			//Test out parameter 
			(string path, string extension) testArgs;
			Consoleview.ValidateArguments((null, null), mockRelativeFileSystem, out testArgs);
			Assert.IsTrue(testArgs.path == "" && testArgs.extension == "");

			Consoleview.ValidateArguments(("not a path", "not an <tension"), mockRelativeFileSystem, out testArgs);
			Assert.IsTrue(testArgs.path == "" && testArgs.extension == "");

			Consoleview.ValidateArguments(($"{TestData.uncPath}{"\\Fail.txt"}", TestData.txtExt), mockRelativeFileSystem, out testArgs);
			Assert.IsTrue(testArgs.path == "" && testArgs.extension == TestData.txtExt);

			Consoleview.ValidateArguments((TestData.relativePath, "not an ?extension"), mockRelativeFileSystem, out testArgs);
			Assert.IsTrue(testArgs.path == TestData.relativePath && testArgs.extension == "");
		}

		[TestMethod]
		public void Test_Valid_ValidateArgs() {
			(string path, string extension) testArgs;
			Assert.IsTrue(Consoleview.ValidateArguments((TestData.relativePath, TestData.txtExt), mockRelativeFileSystem, out testArgs));
			Assert.IsTrue(testArgs.path == TestData.relativePath && testArgs.extension == TestData.txtExt);

			Assert.IsTrue(Consoleview.ValidateArguments((TestData.relativePath, "txt"), mockRelativeFileSystem, out testArgs));
			Assert.IsTrue(testArgs.path == TestData.relativePath && testArgs.extension == TestData.txtExt);

			Assert.IsTrue(Consoleview.ValidateArguments((TestData.relativePath, "*.txt"), mockRelativeFileSystem, out testArgs));
			Assert.IsTrue(testArgs.path == TestData.relativePath && testArgs.extension == TestData.txtExt);

			Assert.IsTrue(Consoleview.ValidateArguments((TestData.absolutePath, TestData.txtExt), mockAbsoluteFileSystem, out testArgs));
			Assert.IsTrue(testArgs.path == TestData.absolutePath && testArgs.extension == TestData.txtExt);

			Assert.IsTrue(Consoleview.ValidateArguments((TestData.uncPath, TestData.txtExt), mockUNCFileSystem, out testArgs));
			Assert.IsTrue(testArgs.path == TestData.uncPath && testArgs.extension == TestData.txtExt);

			Assert.IsTrue(Consoleview.ValidateArguments((TestData.relativePath, TestData.rtfExt), mockRelativeFileSystem, out testArgs));
			Assert.IsTrue(testArgs.path == TestData.relativePath && testArgs.extension == TestData.rtfExt);

			Assert.IsTrue(Consoleview.ValidateArguments((TestData.absolutePath, TestData.rtfExt), mockAbsoluteFileSystem, out testArgs));
			Assert.IsTrue(testArgs.path == TestData.absolutePath && testArgs.extension == TestData.rtfExt);

			Assert.IsTrue(Consoleview.ValidateArguments((TestData.uncPath, TestData.rtfExt), mockUNCFileSystem, out testArgs));
			Assert.IsTrue(testArgs.path == TestData.uncPath && testArgs.extension == TestData.rtfExt);
		}

		/**
			* 1. figureout how to test a UNC file path
			* 2. Whats the difference between ascII and UTF-8 and what are Windows line separators(CR LF)
			*/
	}

}
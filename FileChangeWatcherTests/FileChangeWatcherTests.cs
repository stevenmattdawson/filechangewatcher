﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.IO.Abstractions.TestingHelpers;
using System.Diagnostics;
using FileChangeWatcher;
using FileChangeWatcher.EventInfo;
using System.Text;
using Tests;

namespace FileChangeWatcher_UnitTests {
	[TestClass]
	public class LineChangeWatcherTests {
		MockFileSystem relative;
		MockFileSystem absolute;
		MockFileSystem UNC;

		TestTraceListener trace;

		[TestInitialize]
		public void SetupForTests() {
			relative = (MockFileSystem)TestData.GetMockFileSystem(TestData.PathType.Relative);
			absolute = (MockFileSystem)TestData.GetMockFileSystem(TestData.PathType.Absolute);
			UNC = (MockFileSystem)TestData.GetMockFileSystem(TestData.PathType.UNC);

			trace = new TestTraceListener();
			Trace.Listeners.Add(trace);

			Directory.CreateDirectory(TestData.directory);
		}

		[TestCleanup]
		public void DisposeForTests() {
			TestData.CleanUpDirectory();
		}

		#region Proccess Create/Change/Delete Tests
		[TestMethod]
		public async Task Test_ProcessFileCreated() {
			var watcher = GetLineChangeWatcher(TestData.absolutePath, TestData.txtExt, absolute);
			TestData.testPath = TestData.absolutePath;
			var mock1 = TestData.GetMockFileData(7);
			var mock2 = TestData.GetMockFileData(0);
			var mock3 = TestData.GetMockFileData(10000);
			var mock4 = TestData.GetMockFileData(5000);

			string path1 = $@"{TestData.testPath}\Test1.txt";
			string path2 = $@"{TestData.testPath}\Test2.txt";
			string path3 = $@"{TestData.testPath}\Test3.txt";
			string path4 = $@"{TestData.testPath}\Test4.txt";

			TestData.AddFile(absolute, path1, mock1);
			TestData.AddFile(absolute, path2, mock2);
			TestData.AddFile(absolute, path3, mock3);
			TestData.AddFile(absolute, path4, mock4);

			var result = await watcher.ProcessFileCreated(path1);
			var expectedResult = new CreatedEventInfo(absolute.Path.GetFileName(path1), 7);
			Assert.AreEqual(expectedResult, result);

			result = await watcher.ProcessFileCreated(path2);
			expectedResult = new CreatedEventInfo(absolute.Path.GetFileName(path2), 0);
			Assert.AreEqual(expectedResult, result);

			result = await watcher.ProcessFileCreated(path3);
			expectedResult = new CreatedEventInfo(absolute.Path.GetFileName(path3), 10000);
			Assert.AreEqual(expectedResult, result);

			result = await watcher.ProcessFileCreated(path4);
			expectedResult = new CreatedEventInfo(absolute.Path.GetFileName(path4), 5000);
			Assert.AreEqual(expectedResult, result);
		}

		[TestMethod]
		public async Task Test_ProcessFileChanged() {
			var watcher = GetLineChangeWatcher(TestData.absolutePath, TestData.txtExt, absolute);
			TestData.testPath = TestData.absolutePath;
			string file1 = TestData.FirstPath;
			string file2 = TestData.SecondPath;
			string file3 = TestData.ThirdPath;
			string file4 = TestData.FourthPath;

			string[] newFile1 = new string[] { "line1", "line2", "line3", "line4", "line5" };
			string[] newFile2 = new string[] { "line1", "line2", "line3", "line4", "line5", "line6", "line7", "line 8", "line 9", "line 10", "line 11" };
			string[] newFile3 = new string[] { "line1", "line2", "line3", "line4", "line5", "line6", "line7" };
			string[] newFile4 = new string[] { };

			int lineCount1 = absolute.File.ReadLines(file1).Count();
			int lineCount2 = absolute.File.ReadLines(file2).Count();
			int lineCount3 = absolute.File.ReadLines(file3).Count();
			int lineCount4 = absolute.File.ReadLines(file4).Count();


			absolute.File.WriteAllLines(file1, newFile1);
			absolute.File.SetLastWriteTime(file1, DateTime.Now.AddSeconds(7));
			absolute.File.WriteAllLines(file2, newFile2);
			absolute.File.SetLastWriteTime(file2, DateTime.Now.AddSeconds(7));
			absolute.File.WriteAllLines(file3, newFile3);
			absolute.File.SetLastWriteTime(file3, DateTime.Now.AddSeconds(7));
			absolute.File.WriteAllLines(file4, newFile4);
			absolute.File.SetLastWriteTime(file4, DateTime.Now.AddSeconds(7));

			//because WriteAllLines apends a new line on each line written, we account for that with an offset in the comparison
			var result = await watcher.ProcessFileChanged(file1);
			var expectedResult = new ChangedEventInfo(absolute.Path.GetFileName(file1), newFile1.Count() + 1, lineCount1);
			Assert.AreEqual(expectedResult, result);

			result = await watcher.ProcessFileChanged(file2);
			expectedResult = new ChangedEventInfo(absolute.Path.GetFileName(file2), newFile2.Count() + 1, lineCount2);
			Assert.AreEqual(expectedResult, result);

			result = await watcher.ProcessFileChanged(file3);
			expectedResult = new ChangedEventInfo(absolute.Path.GetFileName(file3), newFile3.Count() + 1, lineCount3);
			Assert.AreEqual(expectedResult, result);

			result = await watcher.ProcessFileChanged(file4);
			expectedResult = new ChangedEventInfo(absolute.Path.GetFileName(file4), newFile4.Count(), lineCount4);
			Assert.AreEqual(expectedResult, result);
		}

		[TestMethod]
		public async Task Test_ProcessFileDeleted() {
			var watcher = GetLineChangeWatcher(TestData.absolutePath, TestData.txtExt, absolute);
			TestData.testPath = TestData.absolutePath;
			string file1 = TestData.FirstPath;
			string file2 = TestData.SecondPath;
			string file3 = TestData.ThirdPath;
			string file4 = TestData.FourthPath;

			absolute.File.Delete(file1);
			absolute.File.Delete(file2);
			absolute.File.Delete(file3);
			absolute.File.Delete(file4);

			var result = await watcher.ProcessFileDeleted(file1);
			var expectedResult = new DeletedEventInfo(TestData.FirstFileName);
			Assert.AreEqual(expectedResult, result.ToString());

			result = await watcher.ProcessFileDeleted(file2);
			expectedResult = new DeletedEventInfo(TestData.SecondFileName);
			Assert.AreEqual(expectedResult, result.ToString());

			result = await watcher.ProcessFileDeleted(file3);
			expectedResult = new DeletedEventInfo(TestData.ThirdFileName);
			Assert.AreEqual(expectedResult, result.ToString());

			result = await watcher.ProcessFileDeleted(file4);
			expectedResult = new DeletedEventInfo(TestData.FourthFileName);
			Assert.AreEqual(expectedResult, result.ToString());
		}
		#endregion

		#region FilePath Tests
		private async Task TestFileCreated(string rootPath, MockFileSystem system) {
			var watcher = GetLineChangeWatcher(rootPath, TestData.txtExt, system);
			var mockInputFile = TestData.GetMockFileData(7);
			TestData.AddFile(system, $@"{rootPath}\Test.txt", mockInputFile);

			Assert.IsTrue(await watcher.PerformCheckAsync());
			var test = new CreatedEventInfo("Test.txt", 7);
			Assert.AreEqual(trace.MostRecent, test.ToString(), $"\n---Trace:---\n{trace}");
		}

		[TestMethod]
		public async Task Test_FileCreated_RelativePathAsync() {
			await TestFileCreated(TestData.relativePath, relative);
		}
		[TestMethod]
		public async Task Test_FileCreated_AbsolutePathAsync() {
			await TestFileCreated(TestData.absolutePath, absolute);
		}
		[TestMethod]
		public async Task Test_FileCreated_UNCPathAsync() {
			await TestFileCreated(TestData.uncPath, UNC);
		}

		private async Task TestFileChanged(string rootPath, MockFileSystem system) {
			var watcher = GetLineChangeWatcher(rootPath, TestData.txtExt, system);
			List<string> newFile = new List<string>() { "line1", "line2", "line3", "line4", "line5" };
			string file = $"{rootPath}\\First.txt";
			int lineCount = system.File.ReadLines(file).Count();
			system.File.WriteAllLines(file, newFile);
			system.File.SetLastWriteTime(file, DateTime.Now.AddSeconds(7));

			Assert.IsTrue(await watcher.PerformCheckAsync());
			var test = new ChangedEventInfo("First.txt", newFile.Count() + 1, lineCount);
			Assert.AreEqual(trace.MostRecent, test.ToString(), $"\n---Trace:---\n{trace}");
		}
		[TestMethod]
		public async Task Test_FileChanged_RelativePathAsync() {
			await TestFileChanged(TestData.relativePath, relative);
		}
		[TestMethod]
		public async Task Test_FileChanged_AbsolutePathAsync() {
			await TestFileChanged(TestData.absolutePath, absolute);
		}
		[TestMethod]
		public async Task Test_FileChanged_UNCPathAsync() {
			await TestFileChanged(TestData.uncPath, UNC);
		}

		private async Task TestFileDeleted(string rootPath, MockFileSystem system) {
			var watcher = GetLineChangeWatcher(rootPath, TestData.txtExt, system);
			string file = $@"{rootPath}\Second.txt";
			system.File.Delete(file);

			Assert.IsTrue(await watcher.PerformCheckAsync());
			var test = new DeletedEventInfo("Second.txt");
			Assert.AreEqual(trace.MostRecent, test.ToString(), $"\n---Trace:---\n{trace}");
		}

		[TestMethod]
		public async Task Test_FileDeleted_RelativePathAsync() {
			await TestFileDeleted(TestData.relativePath, relative);
		}
		[TestMethod]
		public async Task Test_FileDeleted_AbsolutePathAsync() {
			await TestFileDeleted(TestData.absolutePath, absolute);
		}
		[TestMethod]
		public async Task Test_FileDeleted_UNCPathAsync() {
			await TestFileDeleted(TestData.uncPath, UNC);
		}

		[TestMethod]
		public async Task Test_DirectoryCreation() {
			var watcher = GetLineChangeWatcher(TestData.relativePath, TestData.txtExt, relative);

			relative.AddDirectory("TestDirectory");

			Assert.IsTrue(await watcher.PerformCheckAsync());
			Assert.IsTrue(trace.MostRecent == "", $"Nothing should happen when a directory is created");
		}

		#endregion

		#region Load Tests

		[TestMethod]
		public async Task Test_MultipleEdits() {
			var watcher = GetLineChangeWatcher(TestData.relativePath, TestData.rtfExt, relative);
			var mockInputFile = TestData.GetMockFileData(7);
			string path = TestData.relativePath;
			TestData.testPath = TestData.relativePath;
			TestData.testExt = TestData.rtfExt;
			string file1 = TestData.FirstFileName;
			string file2 = TestData.SecondFileName;
			string file3 = TestData.ThirdFileName;
			string file4 = TestData.FourthFileName;

			TestData.AddFile(relative, TestData.FirstPath, mockInputFile);
			TestData.AddFile(relative, TestData.SecondPath, mockInputFile);
			TestData.AddFile(relative, TestData.ThirdPath, mockInputFile);
			TestData.AddFile(relative, TestData.FourthPath, mockInputFile);

			int mockfileCount = relative.File.ReadAllLines(TestData.FirstPath).Count();

			Assert.IsTrue(await watcher.PerformCheckAsync());
			Assert.IsTrue(trace.Count == 4);
			AssertTraceMessageExists(new CreatedEventInfo(file1, mockfileCount));
			AssertTraceMessageExists(new CreatedEventInfo(file2, mockfileCount));
			AssertTraceMessageExists(new CreatedEventInfo(file3, mockfileCount));
			AssertTraceMessageExists(new CreatedEventInfo(file4, mockfileCount));

			string file5 = "Five.rtf";
			string file6 = "Six.rtf";
			TestData.AddFile(relative, $@"{path}\{file5}", mockInputFile);
			TestData.AddFile(relative, $@"{path}\{file6}", mockInputFile);

			List<string> newFile1 = new List<string>() { "line1", "line2", "line3", "line4", "line5" };
			List<string> newFile2 = new List<string>() { "line1", "line2", "line3", "line4", "line5", "line6", "line7", "line8", "line9", "line10" };
			relative.File.Delete($@"{path}\{file3}");
			List<string> newFile4 = new List<string>() { };

			int file1Count = mockfileCount;
			int file2Count = mockfileCount;
			int file4Count = mockfileCount;

			TestData.WriteLines(relative, TestData.FirstPath, newFile1, 7);
			TestData.WriteLines(relative, TestData.SecondPath, newFile2, 7);
			TestData.WriteLines(relative, TestData.FourthPath, newFile4, 7);

			Assert.IsTrue(await watcher.PerformCheckAsync());
			Assert.IsTrue(trace.Count == 10, $"Count was {trace.Count}\n---Trace:---\n{trace}");
			AssertTraceMessageExists(new CreatedEventInfo(file5, mockfileCount));
			AssertTraceMessageExists(new CreatedEventInfo(file6, mockfileCount));
			//because WriteAllLines apends a new line on each line written, we account for that with an offset in the comparison
			AssertTraceMessageExists(new ChangedEventInfo(file1, newFile1.Count() + 1, file1Count));
			AssertTraceMessageExists(new ChangedEventInfo(file2, newFile2.Count() + 1, file2Count));
			AssertTraceMessageExists(new ChangedEventInfo(file4, newFile4.Count(), file4Count));
			AssertTraceMessageExists(new DeletedEventInfo(file3));
		}

		[TestMethod]
		public async Task Test_Large_FileSize() {
			string path = $@"{TestData.relativePath}\superlongline.txt";
			var watcher = GetLineChangeWatcher(TestData.relativePath, TestData.txtExt, relative);
			using (MockFileStream stream = (MockFileStream)relative.FileStream.Create(path, FileMode.CreateNew)) {
				stream.Write(new byte[] { (byte)'\r', (byte)'\n', (byte)'\r', (byte)'\n' }, 0, 4);
				stream.SetLength((long)268435456); // i get an outofmemory exception here if i try to go larger than this....
				TestData.AddFile(relative, path, new MockFileData(stream.GetBuffer()));
				relative.File.SetLastWriteTime(path, DateTime.Now.AddSeconds(7));
			}
			Assert.IsTrue(await watcher.PerformCheckAsync());
			Assert.IsTrue(trace.Count == 1, $"Count was {trace.Count}\n---Trace:---\n{trace}");
			AssertTraceMessageExists(new CreatedEventInfo("superlongline.txt", 3));
		}

		[TestMethod]
		public async Task Test_2GB_FileSize() {
			string path = $@"{TestData.directory}\largefile.txt";
			var watcher = GetLineChangeWatcher(TestData.directory, TestData.txtExt);
			using (FileStream stream = new FileStream(path, FileMode.CreateNew, FileAccess.ReadWrite, FileShare.None)) {
				stream.Write(new byte[] { (byte)'\r', (byte)'\n', (byte)'\r', (byte)'\n' }, 0, 4);
				stream.SetLength((long)268435456); // i get an outofmemory exception here if i try to go larger than this....
			}
			Assert.IsTrue(await watcher.PerformCheckAsync());
			Assert.IsTrue(trace.Count == 1, $"Count was {trace.Count}\n---Trace:---\n{trace}");
			AssertTraceMessageExists(new CreatedEventInfo("largefile.txt", 3));
		}
		#endregion

		#region Locked File Tests
		[TestMethod]
		public async Task Test_FileLocked_Created() {
			//the mock file system doesn't lock files correctly...
			var watcher = GetLineChangeWatcher(TestData.directory, TestData.txtExt);

			string path1 = $@"{TestData.directory}\Test_FileLocked_Created1.txt";
			string path2 = $@"{TestData.directory}\Test_FileLocked_Created2.txt";
			string absolutePath1 = Path.GetFullPath(path1);
			string absolutePath2 = Path.GetFullPath(path2);

			var file1 = File.CreateText(path1);
			var file2 = File.CreateText(path2);

			file1.WriteLine("test");
			file2.WriteLine("test");
			file2.WriteLine("test2");

			file2.Close();

			var lockedResult = await watcher.ProcessFileCreated(absolutePath1);
			var expectedLockedResult = new CreatedFileLockedEvent(Path.GetFileName(absolutePath1));
			var result2 = await watcher.ProcessFileCreated(absolutePath2);
			var expectedResult2 = new CreatedEventInfo(Path.GetFileName(absolutePath2), 3);
			Assert.AreEqual(expectedLockedResult, lockedResult);
			Assert.AreEqual(expectedResult2, result2);

			file1.Close();

			var result = await watcher.ProcessFileCreated(absolutePath1);
			var expectedResult = new CreatedEventInfo(Path.GetFileName(absolutePath1), 2);
			Assert.AreEqual(expectedResult, result);
		}

		[TestMethod]
		public async Task Test_FileLocked_Changed() {
			//the mock file system doesn't lock files correctly...
			string path1 = $@"{TestData.directory}\Test_FileLocked_Changed1.txt";
			string path2 = $@"{TestData.directory}\Test_FileLocked_Changed2.txt";
			string absolutePath1 = Path.GetFullPath(path1);
			string absolutePath2 = Path.GetFullPath(path2);

			var file1 = File.CreateText(path1);
			var file2 = File.CreateText(path2);
			file2.Close();
			file1.Close();

			var watcher = GetLineChangeWatcher(TestData.directory, TestData.txtExt);

			FileStream stream1 = File.OpenWrite(path1);
			FileStream stream2 = File.OpenWrite(path2);

			stream1.Write(Encoding.ASCII.GetBytes("text"), 0, 4);
			stream2.Write(Encoding.ASCII.GetBytes("text"), 0, 4);

			stream2.Close();

			var lockedResult = await watcher.ProcessFileChanged(absolutePath1);
			var expectedLockedResult = new ChangedFileLockedEventInfo(Path.GetFileName(absolutePath1));
			var result2 = await watcher.ProcessFileChanged(absolutePath2);
			var expectedResult2 = new ChangedEventInfo(Path.GetFileName(absolutePath2), 1, 0);
			Assert.AreEqual(expectedLockedResult, lockedResult);
			Assert.AreEqual(expectedResult2, result2.ToString());

			stream1.Close();

			var result = await watcher.ProcessFileChanged(absolutePath1);
			var expectedResult = new ChangedEventInfo(Path.GetFileName(absolutePath1), 1, 0);
			Assert.AreEqual(expectedResult, result.ToString());
		}

		[TestMethod]
		public async Task Test_FileLocked_Initialization() {
			//the mock file system doesn't lock files correctly...
			LineChangeWatcher watcher;
			string path1 = $@"{TestData.directory}\Test_FileLocked_Initialized.txt";
			string path2 = $@"{TestData.directory}\Test_FileLocked_Initialized2.txt";
			string absolutePath2 = Path.GetFullPath(path2);

			var file1 = File.CreateText(path1);
			var file2 = File.CreateText(path2);

			file2.Close();
			watcher = GetLineChangeWatcher(TestData.directory, TestData.txtExt);
			file1.Close();

			FileStream stream1 = File.OpenWrite(path1);
			FileStream stream2 = File.OpenWrite(path2);

			stream1.Write(Encoding.ASCII.GetBytes("text"), 0, 4);
			stream2.Write(Encoding.ASCII.GetBytes("text"), 0, 4);

			stream1.Close();
			stream2.Close();

			_ = await watcher.PerformCheckAsync();
			var expectedResult2 = new ChangedEventInfo(Path.GetFileName(absolutePath2), 1, 0);
			Assert.IsTrue(trace.TestConsole.Contains(Path.GetFullPath(path1)) == false, $"FileWatcher reported information on {path1} before it had been fully initialized\nthis occurs when a file is locked during initailziation.");
			AssertTraceMessageExists(expectedResult2);
		}
		#endregion

		private void AssertTraceMessageExists(EventInfoBase result) {
			Assert.IsTrue(trace.MessageExists(result.ToString()), $"{result} was not found in the tracelistener message history.\n---Trace:---\n{trace}");
		}

		private LineChangeWatcher GetLineChangeWatcher(string filePath, string extension, MockFileSystem fileSystem = null) {
			var watcher = fileSystem == null ? new LineChangeWatcher(filePath, extension) : new LineChangeWatcher(filePath, extension, fileSystem);
			watcher.Initialize();
			return watcher;
		}
	}

}
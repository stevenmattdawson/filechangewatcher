﻿using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.IO.Abstractions.TestingHelpers;
using System.Linq;
using System.Threading.Tasks;
using FileChangeWatcher;
using FileChangeWatcher.EventInfo;
using FileChangeWatcherWindow.Models;
using FileChangeWatcherWindow.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Tests;

namespace FileChangeWatcher_IntegrationTests {

	[TestClass]
	public class FileChangeWatcherWindowTests {

		private ShellViewModel window;
		private LineChangeWatcher watcher {
			get => window.Watcher;
			set => window.Watcher = value;
		}

		MockFileSystem relative;

		[TestInitialize]
		public void SetupForTests() {
			relative = (MockFileSystem)TestData.GetMockFileSystem(TestData.PathType.Relative);
			window = new ShellViewModel(relative, new MockDispatcher());
			window.FilePath = TestData.testPath;
			window.Extension = TestData.testExt;

			Directory.CreateDirectory(TestData.directory);
		}

		[TestCleanup]
		public void DisposeForTests() {
			TestData.CleanUpDirectory();
		}




		[TestMethod]
		public void Test_CanBeginWatching() {
			Assert.IsTrue(window.CanBeginWatching, "With a valid filepath and extension the button should be enabled");
			Assert.AreEqual(window.FilePathBorder.Color, window.ValidColor);
			Assert.AreEqual(window.ExtensionBorder.Color, window.ValidColor);

			window.FilePath = "lakjsdf";
			Assert.IsFalse(window.CanBeginWatching, "With an invalid Filepath, the button should be disabled");
			Assert.AreEqual(window.FilePathBorder.Color, window.InvalidColor);

			window.FilePath = TestData.testPath;
			window.Extension = @"lak/jsdf";
			Assert.IsFalse(window.CanBeginWatching, "With an invalid Extension, the button should be disabled");
			Assert.AreEqual(window.FilePathBorder.Color, window.ValidColor, "With a valid path once again, this should be a valid color");
			Assert.AreEqual(window.ExtensionBorder.Color, window.InvalidColor);

			window.Extension = TestData.testExt;
			Assert.IsTrue(window.CanBeginWatching, "With an valid Extension, the button should be enabled");
			Assert.AreEqual(window.ExtensionBorder.Color, window.ValidColor, "With a valid extension once again, this should be a valid color");
		}

		[TestMethod]
		public void Test_ClearOnBeginWatching() {
			window.Files.Clear();
			window.Files.Add(new FileModel());
			window.ConsoleBox.Clear();
			window.ConsoleBox.Add(new ConsoleMessageViewModel());
			window.BeginWatching();
			Assert.IsFalse(window.Files.Any(), "Begin Watching should clear the files and consolebox before starting");
			Assert.IsFalse(window.ConsoleBox.Any(), "BeginWatching shoudl clear the files and consolebox before starting");
		}

		private void PrepareForConsoleBoxTests() {
			window.ConsoleBox.Clear();
			watcher = new LineChangeWatcher(TestData.testPath, TestData.testExt, relative);
			watcher.FileChangedEvent += window.OnUpdateEventInfo;
			watcher.Initialize();
		}

		[TestMethod]
		public async Task Test_ConsoleBoxEvents_CreatedFileEventAsync() {
			PrepareForConsoleBoxTests();

			var mock1 = TestData.GetMockFileData(7);
			string path1 = $@"{TestData.testPath}\Test1.txt";
			TestData.AddFile(relative, path1, mock1);

			await watcher.PerformCheckAsync();

			Assert.IsTrue(window.ConsoleBox.Any(), "We didn't get the CreatedFileEvent Message");
			var expectedEvent = new CreatedEventInfo("Test1.txt", 7);
			Assert.AreEqual(expectedEvent.ToString(), window.ConsoleBox[0].Info);
		}



		[TestMethod]
		public async Task Test_ConsoleBoxEvents_ChangedFileEventAsync() {
			PrepareForConsoleBoxTests();

			List<string> newFile1 = new List<string>() { "line1", "line2", "line3", "line4", "line5" };
			int mockfileCount = relative.File.ReadAllLines(TestData.FirstPath).Count();
			TestData.WriteLines(relative, TestData.FirstPath, newFile1, 7);
			int newCount = relative.File.ReadAllLines(TestData.FirstPath).Count();

			await watcher.PerformCheckAsync();

			Assert.IsTrue(window.ConsoleBox.Any(), "We didn't get the ChangedEvent Message");
			var expectedEvent = new ChangedEventInfo(TestData.FirstFileName, newCount + 1, mockfileCount);
			Assert.AreEqual(expectedEvent.ToString(), window.ConsoleBox[0].Info);
		}

		[TestMethod]
		public async Task Test_ConsoleBoxEvents_DeletedFileEventAsync() {
			PrepareForConsoleBoxTests();
			
			relative.File.Delete(TestData.FirstPath);

			await watcher.PerformCheckAsync();

			Assert.IsTrue(window.ConsoleBox.Any(), "We didn't get the DeletedEvent Message");
			var expectedEvent = new DeletedEventInfo(TestData.FirstFileName);
			Assert.AreEqual(expectedEvent.ToString(), window.ConsoleBox[0].Info);
		}


		[TestMethod]
		public async Task Test_ConsoleBox_LockedFileEvent() {
			window = new ShellViewModel(new FileSystem(), new MockDispatcher()); // the mockfilesystem doesn't lock files correctly.
			window.ConsoleBox.Clear();
			string path = $@"{TestData.directory}\Test_ConsoleBox_LockedFileEvent1.txt";
			watcher = new LineChangeWatcher(TestData.directory, ".txt");
			watcher.FileChangedEvent += window.OnUpdateEventInfo;
			watcher.Initialize();
			using (var file = File.CreateText(path)) {

				await watcher.PerformCheckAsync();

				Assert.IsTrue(window.ConsoleBox.Any());
				var expectedEvent = new CreatedFileLockedEvent("Test_ConsoleBox_LockedFileEvent1.txt");
				Assert.AreEqual(expectedEvent.ToString(), window.ConsoleBox[0].Info);
				file.Close();
			}
		}

		[TestMethod]
		public void Test_FilesDataGrid_Initialization() {
			window.Files.Clear();
			watcher = new LineChangeWatcher(TestData.testPath, TestData.testExt, relative);
			watcher.FileInitializedEvent += window.OnInitializeFileInfo;
			watcher.Initialize();
			Assert.IsTrue(window.Files.Any(), "We didn't get the OnInitializeFileInfo Message");
			
		}

		[TestMethod]
		public async Task Test_FilesDataGrid_EventsAsync() {
			window.Files.Clear();
			watcher = new LineChangeWatcher(TestData.testPath, TestData.testExt, relative);
			watcher.FileInitializedEvent += window.OnInitializeFileInfo;
			watcher.FileChangedEvent += window.OnUpdateEventInfo;
			watcher.Initialize();
			Assert.IsTrue(window.Files.Any(), "We didn't get the OnInitializeFileInfo Message");
			int currentCount = window.Files.Count();

			var mock1 = TestData.GetMockFileData(7);
			string path1 = $@"{TestData.testPath}\Test1.txt";
			TestData.AddFile(relative, path1, mock1);

			await watcher.PerformCheckAsync();
			Assert.AreEqual(currentCount+1, window.Files.Count(), "We didn't get the Update Event Info Message");
		}

		[TestMethod]
		public void Test_ProgressBar() {
			watcher = new LineChangeWatcher(TestData.testPath, TestData.testExt, relative);
			watcher.UpdateProgressEvent += window.OnUpdateProgressMessage;

			window.ProgressLabel = "Test";
			Assert.IsTrue(window.Progress == 0, "progress should start at 0");

			watcher.Initialize();

			Assert.AreNotEqual("Test", window.ProgressLabel, $"Progress Label: {window.ProgressLabel}");



		}

	
	}
}




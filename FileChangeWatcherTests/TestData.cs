﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.IO.Abstractions.TestingHelpers;
using System.Text;

namespace Tests {

	/// <summary>
	/// Utility class for getting and setting filesystem data for testing.
	/// </summary>
	internal static class TestData
	{
		public const string relativePath = @"..\..\TestFiles";
		private static string _absolutePath = "";
		public const string uncPath = @"\\localhost\\Temp";
		public const string txtExt = ".txt";
		public const string rtfExt = ".rtf";

		public static string testPath = "";
		public static string testExt = "";

		public static string directory = @"..\..\TempUnitTests";

		public static string FirstFileName {
			get => $"First{testExt}";
		}
		public static string SecondFileName {
			get => $"Second{testExt}";
		}
		public static string ThirdFileName {
			get => $"Third{testExt}";
		}

		public static string FourthFileName {
			get => $"Fourth{testExt}";
		}

		public static string FirstPath {
			get => $@"{testPath}\{FirstFileName}";
		}



		public static string SecondPath {
			get => $@"{testPath}\{SecondFileName}";
		}
		public static string ThirdPath {
			get => $@"{testPath}\{ThirdFileName}";
		}
		public static string FourthPath {
			get => $@"{testPath}\{FourthFileName}";
		}


		public static string absolutePath {
			get {
				if (string.IsNullOrEmpty(_absolutePath))
				{
					_absolutePath = Path.GetFullPath(relativePath);
				}
				return _absolutePath;
			}
		}

		public enum PathType
		{
			Relative,
			Absolute,
			UNC
		}

		public static string GetRootPath(PathType type)
		{
			switch (type)
			{
				case PathType.Relative:
					return relativePath;
				case PathType.Absolute:
					return absolutePath;
				case PathType.UNC:
					return uncPath;
			}
			return null; //error case
		}

		public static IFileSystem GetMockFileSystem(PathType type, string extension = ".txt")
		{
			testPath = GetRootPath(type);
			testExt = extension;
			var mockFileSystem = new MockFileSystem();

			var mockInputFile = GetMockFileData(7);

			AddFile(mockFileSystem, FirstPath, mockInputFile);
			AddFile(mockFileSystem, SecondPath, mockInputFile);
			AddFile(mockFileSystem, ThirdPath, mockInputFile);
			AddFile(mockFileSystem, FourthPath, mockInputFile);
			mockFileSystem.AddDirectory($@"{testPath}\SubFiles");
			AddFile(mockFileSystem, $@"{testPath}\SubFiles\First.txt", mockInputFile);
			AddFile(mockFileSystem, $@"{testPath}\SubFiles\Second.txt", mockInputFile);
			AddFile(mockFileSystem, $@"{testPath}\SubFiles\Third.txt", mockInputFile);
			AddFile(mockFileSystem, $@"{testPath}\SubFiles\Fourth.txt", mockInputFile);
			return mockFileSystem;
		}

		public static MockFileData GetMockFileData(int lineCount) {
			StringBuilder fileData = new StringBuilder();
			for (int i = 0; i < lineCount; i++) {
				if (i == lineCount - 1) {
					fileData.AppendFormat("Line{0}", i);
				}
				else {
					fileData.AppendFormat("line {0}\r\n", i);
				}
			}
			return new MockFileData(fileData.ToString());
		}

		public static void AddFile(MockFileSystem system, string path, MockFileData data) {
			system.AddFile(path, data);
			system.File.SetLastWriteTime(path, DateTime.Now);
		}

		public static void WriteLines(MockFileSystem system, string path, List<string> newLines, int seconds) {
			system.File.WriteAllLines(path, newLines);
			system.File.SetLastWriteTime(path, DateTime.Now.AddSeconds(seconds));
		}

		public static void CleanUpDirectory() {
			if (Directory.Exists(directory)) {
				var files = Directory.GetFiles(directory);
				foreach (var file in files) {
					try {
						File.Delete(file);
					}
					catch (IOException) { }
				}
				try {
					Directory.Delete(directory);
				}
				catch (IOException) {

				}
			}
		}
	}
}

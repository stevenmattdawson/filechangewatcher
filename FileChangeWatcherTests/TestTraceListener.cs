﻿using System.Collections.Generic;
using System.Diagnostics;

namespace Tests {

	/// <summary>
	/// Utility class that can respond to trace statements and is used for Unit test assert and debug.
	/// </summary>
	class TestTraceListener : TraceListener {

		public string TestConsole = "";
		public string MostRecent = "";
		public int Count = 0;

		public List<string> Messages = new List<string>();

		public override void Write(string message) {
			TestConsole += message;
			MostRecent = message;
		}

		public override void WriteLine(string message) {
			TestConsole += message + "\n";
			MostRecent = message;
			Messages.Add(message);
			Count++;
		}

		public bool MessageExists(string message) {
			return Messages.Contains(message);
		}

		public override string ToString() {
			return TestConsole;
		}
	}
}

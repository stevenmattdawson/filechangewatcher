﻿namespace FileChangeWatcherWindow.Models {
	public class FileModel {
		public string FileName { get; set; }
		public string FileSize { get; set; }
		public string LastWriteTime { get; set; }
		public string Status { get; set; }
	}
}

﻿using Caliburn.Micro;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace FileChangeWatcherWindow.ViewModels {
	public class ConsoleMessageViewModel : Screen {


		private string _icon;
		public string Icon {
			get { return _icon; }
			set {
				_icon = value;
				NotifyOfPropertyChange(() => Icon);
			}
		}

		private string _info;

		public string Info {
			get {
				return _info;
			}
			set {
				_info = value;
				NotifyOfPropertyChange(() => Info);
			}
		}

	}
}

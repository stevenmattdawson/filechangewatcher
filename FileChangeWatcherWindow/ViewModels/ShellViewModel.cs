﻿using Caliburn.Micro;
using FileChangeWatcher;
using FileChangeWatcherWindow.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Abstractions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Threading;
using Action = System.Action;
using FileChangeWatcher.EventInfo;
using System.Diagnostics;

namespace FileChangeWatcherWindow.ViewModels {
	public class ShellViewModel : Caliburn.Micro.Screen, IDisposable {

		#region Member Variables
		public Color ValidColor => Color.FromRgb(0, 255, 0);
		public Color InvalidColor => Color.FromRgb(255, 0, 0);

		private string _resourceFolder => Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName, "Resources");
		private string _CreatedFileIcon => $@"{_resourceFolder}\CreatedFileIcon.png";
		private string _ChangedFileIcon => $@"{_resourceFolder}\ChangedFileIcon.png";
		private string _DeletedFileIcon => $@"{_resourceFolder}\DeletedFileIcon.png";
		private string _UpToDateIcon => $@"{_resourceFolder}\UpToDateIcon.png";
		private string _LockedFileIcon => $@"{_resourceFolder}\LockedFileIcon.png";

		//provided for unit testing purposes.
		public LineChangeWatcher Watcher { get; set; }

		IFileSystem _fileSystem;
		
		IDispatcher threadDispatcher; 
		#endregion

		public ShellViewModel() : this(new FileSystem(), new UIDispatcher(Dispatcher.CurrentDispatcher)) {
	
		}

		public ShellViewModel(IFileSystem fileSystem, IDispatcher dispatcher) {
			ConsoleBox.Clear();
			threadDispatcher = dispatcher;
			_fileSystem = fileSystem;
		}

		/// <summary>
		/// Included primarily for ease of unit testing.
		/// </summary>
		

		#region Bindable Parameters

		private string _filePath = @"C:\";
		public string FilePath {
			get { return _filePath; }
			set {
				_filePath = value;
				NotifyOfPropertyChange(() => FilePath);
				NotifyOfPropertyChange(() => CanBeginWatching);
			}
		}

		private string _extension = ".txt";
		public string Extension {
			get { return _extension; }
			set {
				_extension = value;
				NotifyOfPropertyChange(() => Extension);
				NotifyOfPropertyChange(() => CanBeginWatching);

			}
		}

		private List<int> _intervals = new List<int>() { 5, 10, 15, 20 };
		public List<int> Intervals {
			get => _intervals;
			set {
				_intervals = value;
				NotifyOfPropertyChange(() => Intervals);
			}
		}

		public int _selectedInterval = 10;
		public int SelectedInterval {
			get => _selectedInterval;
			set => _selectedInterval = value;
		}

		private string _progressLabel;
		public string ProgressLabel {
			get => _progressLabel;
			set {
				_progressLabel = value;
				NotifyOfPropertyChange(() => ProgressLabel);
			}
		}

		private int _progress;
		public int Progress {
			get => _progress;
			set {
				_progress = value;
				NotifyOfPropertyChange(() => Progress);
			}
		}

		BindableCollection<ConsoleMessageViewModel> _consoleBox = new BindableCollection<ConsoleMessageViewModel>();
		public BindableCollection<ConsoleMessageViewModel> ConsoleBox {
			get => _consoleBox;
			set {
				_consoleBox = value;
				NotifyOfPropertyChange(() => ConsoleBox);
			}
		}

		BindableCollection<FileModel> _files = new BindableCollection<FileModel>();
		public BindableCollection<FileModel> Files {
			get => _files;
			set {
				_files = value;
				NotifyOfPropertyChange(() => Files);
			}
		}

		private SolidColorBrush _filePathBorder = new SolidColorBrush();
		public SolidColorBrush FilePathBorder {
			get => _filePathBorder; 
			set {
				_filePathBorder = value;
				NotifyOfPropertyChange(() => FilePathBorder);
			}
		}

		public Color FilePathColor {
			set {
				FilePathBorder.Color = value;
				NotifyOfPropertyChange(() => FilePathBorder);
			}
		}

		private SolidColorBrush _extensionBorder = new SolidColorBrush();
		public SolidColorBrush ExtensionBorder {
			get => _extensionBorder;
			set {
				_extensionBorder = value;
				NotifyOfPropertyChange(() => ExtensionBorder);
			}
		}
		public Color ExtensionColor {
			set {
				ExtensionBorder.Color = value;
				NotifyOfPropertyChange(() => ExtensionBorder);
			}
		}

		private string _runButtonText = "Run";

		public string RunButtonText {
			get => _runButtonText; 
			set {
				_runButtonText = value;
				NotifyOfPropertyChange(() => RunButtonText);
			}
		}

		public bool EnableInputStrings {
			get => Watcher?.IsWatching != true;
		}
		#endregion

		#region Input String Tools
		private bool ValidateArguments(out (string path, string extension) validArgs) {
			return Consoleview.ValidateArguments((FilePath, Extension), _fileSystem, out validArgs);
		}

		public void Browse() {
			using (var selectDirectoryDialog = new FolderBrowserDialog()) {
				selectDirectoryDialog.Description = Strings.BrowseTitle;
				selectDirectoryDialog.SelectedPath = Directory.Exists(FilePath) ? FilePath : (Directory.Exists("C:\\") ? "C:\\" : "");
				if (selectDirectoryDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
					FilePath = selectDirectoryDialog.SelectedPath;
				}
			}
		}

		Task watching = null;
		

		bool _initializationStarted = false;
		/// <summary>
		/// Caliburn.Micro magic makes this enable/disable the BeginWatching Button in the Xaml
		/// </summary>
		public bool CanBeginWatching {
			get {
				(string path, string extension) validArgs = ("", "");
				bool result = ValidateArguments(out validArgs);
				FilePathBorder.Color = string.IsNullOrWhiteSpace(validArgs.path) ? InvalidColor : ValidColor;
				ExtensionBorder.Color = string.IsNullOrWhiteSpace(validArgs.extension) ? InvalidColor : ValidColor;
				return (_initializationStarted == false) && (result || Watcher?.IsWatching == true);
			}
		}

		public void BeginWatching(bool block = false) {
			(string path, string extension) validArgs = ("", "");
			if (Watcher?.IsWatching != true && ValidateArguments(out validArgs)) {
				Files.Clear();
				ConsoleBox.Clear();
				NotifyOfPropertyChange(() => ConsoleBox);
				NotifyOfPropertyChange(() => Files);

				watching?.Dispose();
				watching = Task.Run(() => {
					Watcher = new LineChangeWatcher(validArgs.path, validArgs.extension, _fileSystem, SelectedInterval * 1000);
					Watcher.FileChangedEvent += OnUpdateEventInfo;
					Watcher.FileInitializedEvent += OnInitializeFileInfo;
					Watcher.UpdateProgressEvent += OnUpdateProgressMessage;
					_initializationStarted = true;
					Watcher.Initialize();
					Watcher.StartWatching();
					_initializationStarted = false;
					NotifyOfPropertyChange(() => EnableInputStrings);
					NotifyOfPropertyChange(() => CanBeginWatching);
				});

				if (block) {
					watching.Wait(SelectedInterval * 1001);
				}

				NotifyOfPropertyChange(() => CanBeginWatching);
				RunButtonText = Strings.StopButtonText;
			}
			else if (watching?.IsCompleted == true) { 
				Watcher.StopWatching();
				NotifyOfPropertyChange(() => EnableInputStrings);
				RunButtonText = Strings.RunButtonText;
			}
		}

		#endregion

		#region View Functionality
		private void AddItemToFiles(EventInfoBase info) {
			ThreadSafeUIEdit(new Action(() => {
				var data = new FileModel() {
					FileName = info.FileName,
					FileSize = info.CurrentLineCount > 0 ? info.CurrentLineCount.ToString() : Strings.ToBeDetermined,
					LastWriteTime = DateTime.Now.ToShortDateString(),
					Status = _CreatedFileIcon
				};
				Files.Add(data);
				NotifyOfPropertyChange(() => Files);
			}));
		}

		private void UpdateItemInFiles(EventInfoBase info, string status) {
			ThreadSafeUIEdit(new Action(() => {
				FileModel model = null;
				foreach (var file in Files) {
					if (file.FileName == info.FileName) {
						Files.Remove(file);
						if (file != null) {
							file.Status = status;
							model = file;
						}
						break;
					}
				}
				if (model != null) {
					Files.Add(model);
				}
				NotifyOfPropertyChange(() => Files);
			}));
		}

		private void RemoveItemFromFiles(EventInfoBase info) {
			ThreadSafeUIEdit(new Action(() => {
				foreach (var file in Files) {
					if (file.FileName == info.FileName) {
						Files.Remove(file);
						break;
					}
				}
				NotifyOfPropertyChange(() => Files);
			}));
		}

		private void ResetIcons() {
			ThreadSafeUIEdit(new Action(() => {
				var newFiles = new BindableCollection<FileModel>();
				foreach (var file in Files) {
					if (file != null) {
						file.Status = _UpToDateIcon;
					}
					newFiles.Add(file);
				}
				Files = newFiles;

			}));
		}

		public void OnUpdateProgressMessage(object sender, ProgressEventInfo info) {
			ThreadSafeUIEdit(new Action(() => {
				ProgressLabel = info.Message;
				Progress = (int)info.ProgressPercentage;
			}));
			NotifyOfPropertyChange(() => CanBeginWatching);
			
		}

		public void OnInitializeFileInfo(object sender, InitializedFileEventInfo info) {
			ThreadSafeUIEdit(new Action(() => {
				var data = new FileModel() {
					FileName = info.FileName,
					FileSize = info.LineCount > 0 ? info.LineCount.ToString() : Strings.ToBeDetermined,
					LastWriteTime = info.LastWriteTime.ToString(),
					Status = info.LineCount > 0 ? _UpToDateIcon : _LockedFileIcon
				};

				Files.Add(data);
				NotifyOfPropertyChange(() => Files);
			}));
		}

		public void AddItemToConsoleBox(EventInfoBase changedInfo, string iconResourcePath) {
			ThreadSafeUIEdit(new Action(() => {
				ConsoleMessageViewModel eventPanel = new ConsoleMessageViewModel();
				eventPanel.Icon = iconResourcePath;
				eventPanel.Info = changedInfo.ToString();
				ConsoleBox.Add(eventPanel);
				NotifyOfPropertyChange(() => ConsoleBox);
			}));
		}

		public void OnUpdateEventInfo(object sender, EventInfoBase info) {
			ResetIcons();
			if (info != null) {
				if (info is ChangedEventInfo changedInfo) {
					AddItemToConsoleBox(changedInfo, _ChangedFileIcon);
					UpdateItemInFiles(changedInfo, _ChangedFileIcon);
				}
				else if (info is CreatedEventInfo createdInfo) {
					AddItemToConsoleBox(createdInfo, _CreatedFileIcon);
					AddItemToFiles(info);
				}
				else if (info is DeletedEventInfo deletedInfo) {
					AddItemToConsoleBox(deletedInfo, _DeletedFileIcon);
					RemoveItemFromFiles(info);
				}
				else if (info is FileLockedEventInfo lockedInfo) {
					AddItemToConsoleBox(lockedInfo, _LockedFileIcon);
					UpdateItemInFiles(lockedInfo, _LockedFileIcon);
				}
			}
		}

		private void ThreadSafeUIEdit(Action editOperation) {
			threadDispatcher.Dispatch(editOperation);
		}



		public void Dispose() {
			try {
				watching?.Dispose();
			}
			catch(Exception e) {
				Trace.WriteLine(e.Message);
			}
		}
		#endregion
	}
}

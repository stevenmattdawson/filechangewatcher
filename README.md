# FileChangeWatcher

C# Project Example utilizing TDD and async functionality

---- Usages:
FileChangeWatcher.exe [rootpath] [file extension]
FileChangeWatcher.exe help
FileChangeWatcher.exe ?

---- Parameters:
[rootpath]			The path to the directory to watch, this can be a relative, absolute, or UNC path.
[file extension]		The file extension to watch for created, changed, and deleted events. 
				Only alphanumeric characters are considered valid in the extension.

---- Help:
help				Displays this message.
?				Displays this message.

---- Quit:
Ctrl^C				Exits the program.



#FileChangeWatcherWindow

A dialogue wrapper for the console application. Provides real time file path and extension validation, customizable scan interval, and the ability to start and stop the scanner, change arguements, then restart.

Also displays a list of files being scanned and icons indicating recent events detected.